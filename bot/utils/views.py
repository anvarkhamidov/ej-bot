from bot.locale.strings import get_emoji as _e, get_text as _s
from bot.utils import core
from web.models import User

import typing


def get_main_menu(user: User) -> typing.Tuple[str, typing.List]:
    text = _s('overview', user.lang)
    buttons = [
        _s('menu_order', user.lang),
        _s('menu_bot_info', user.lang), _s('menu_cart', user.lang)
    ]
    keyboard = core.build_menu(buttons[1:], n_cols=2, header_buttons=buttons[0])
    return text, keyboard
