# coding=utf-8
from threading import Thread
from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.contrib.admin.views.decorators import staff_member_required
from telegram.error import TelegramError
from telegrambot.apps import TelegrambotConfig
import sys
import json
import telegram
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)
manager = settings.TELEGRAM_MANAGER


def get_bot_dispatcher(bot_token):
    bot = TelegrambotConfig.get_bot(bot_id=bot_token, safe=False)
    if bot is None:
        logger.warning('Request for not found token : {}'.format(bot_token))
        return JsonResponse({})

    dispatcher = TelegrambotConfig.get_dispatcher(bot_token, safe=False)
    if dispatcher is None:
        logger.error('Dispatcher for bot <{}> not found : {}'.format(bot.username, bot_token))
        return JsonResponse({})

    return bot, dispatcher


@staff_member_required
def home(request):
    bot_list = TelegrambotConfig.bots
    used_tokens = TelegrambotConfig.used_tokens
    context = {'bot_list': bot_list, 'update_mode': manager.get('MODE'), 'used_tokens': used_tokens}
    return render(request, 'bots/index.html', context)


@csrf_exempt
def webhook(request, bot_token):
    try:
        bot, dispatcher = get_bot_dispatcher(bot_token=bot_token)
    except Exception as e:
        logger.error(f"Error with getting bot and dispatcher: {e}")
        return JsonResponse({})

    if bot.token not in TelegrambotConfig.used_tokens:
        return JsonResponse({})
    try:
        data = json.loads(request.body.decode("utf-8"))
    except:
        logger.warning('Telegram bot <{}> receive invalid request : {}'.format(bot.username, repr(request)))
        return JsonResponse({})

    try:
        update = telegram.Update.de_json(data, bot)
        dispatcher.process_update(update)
        logger.debug('Bot <{}> : Processed update {}'.format(bot.username, update))
    # Dispatch any errors
    except TelegramError as te:
        logger.warning("Bot <{}> : Error was raised while processing Update.".format(bot.username))
        dispatcher.dispatchError(update, te)

    # All other errors should not stop the thread, just print them
    except:
        logger.error("Bot <{}> : An uncaught error was raised while processing an update\n{}".format(bot.username, sys.exc_info()[0]))

    finally:
        return JsonResponse({})


@csrf_exempt
def stop(request, bot_token):
    bot, dispatcher = get_bot_dispatcher(bot_token=bot_token)
    try:
        if bot_token in TelegrambotConfig.used_tokens:
            TelegrambotConfig.remove_token(bot_token)
            logger.info(f"Bot:{bot.username}:stopped")
    except Exception as e:
        logger.error(f"Error while stopping {bot.username}: {e}")
        return redirect('django-telegrambot')

    return redirect('django-telegrambot')


@csrf_exempt
def start(request, bot_token):
    bot, dispatcher = get_bot_dispatcher(bot_token=bot_token)
    try:
        updater = TelegrambotConfig.get_updater(bot_id=bot.token)
        if updater not in TelegrambotConfig.used_tokens:
            TelegrambotConfig.add_token(bot_token)

        def stop_and_restart(_updater):
            _updater.stop()

        Thread(target=stop_and_restart, args=(updater,)).start()
        logger.info(f"Bot:{bot.username}:started")
    except Exception as e:
        logger.error(f"Error while stopping {bot.username}: {e}")
        return JsonResponse({})

    return JsonResponse({})