from django.db import models
from psqlextra.models import PostgresModel
from psqlextra.manager import PostgresManager
from telegrambot.apps import TelegrambotConfig, run_bot_command
import logging

logger = logging.getLogger(__name__)


class Bot(models.Model):
    objects = PostgresManager()
    
    token = models.CharField(verbose_name='Bot token', max_length=255)
    enabled = models.BooleanField(verbose_name='Bot Enabled', default=True)
    allowed_updates = models.TextField(verbose_name='Allowed Updates (array)', default="[]")
    timeout = models.PositiveIntegerField(verbose_name='Timeout', default=1)
    webhook_max_connections = models.IntegerField(verbose_name='Webhook MAX connections', default=40)
    message_queue_enabled = models.BooleanField(verbose_name='Enable MessageQueue', default=False)
    message_all_burst_limit = models.IntegerField(verbose_name='MessageQueue all burst limit', default=29)
    message_all_time_limit_ms = models.IntegerField(verbose_name='MessageQueue all time limit in ms', default=1024)
    message_request_con_pool_size = models.IntegerField(verbose_name='MQ Request connection pool size', default=8)
    poll_interval = models.FloatField(verbose_name='Poll interval', default=0)
    poll_clean = models.BooleanField(verbose_name='Poll clean', default=False)
    poll_bootstrap_retries = models.IntegerField(verbose_name='Poll bootstrap retries', default=0)
    poll_read_latency = models.FloatField(verbose_name='Poll read latency', default=2)
    proxy = models.TextField(verbose_name='Proxy (dict)', default=None, null=True, blank=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        try:
            TelegrambotConfig.process_bot(self)
            current_bot = TelegrambotConfig.get_bot(bot_id=self.token)
            if run_bot_command('main', current_bot.token, True):
                logger.info('Loaded {}'.format(current_bot.username))

            return super(Bot, self).save(force_insert, force_update, using, update_fields)
        except Exception as e:
            logger.error(f"Error while saving object: {e}")
            return

    def delete(self, using=None, keep_parents=False):
        try:
            updater = TelegrambotConfig.get_updater(bot_id=self.token)
            updater.bot.delete_webhook()
            TelegrambotConfig.bots.remove(updater.bot)
            TelegrambotConfig.updaters.remove(updater)
            TelegrambotConfig.dispatchers.remove(updater.dispatcher)
            TelegrambotConfig.bot_tokens.remove(self.token)
            TelegrambotConfig.bot_usernames.remove(updater.bot.username)
            TelegrambotConfig.remove_token(self.token)
            return super(Bot, self).delete(using, keep_parents)
        except Exception as e:
            logger.error(f"Error while deleting object: {e}")
            return

    def __str__(self):
        try:
            bot = TelegrambotConfig.get_bot(bot_id=self.token)
            return f"{bot.username}"
        except Exception as e:
            print('str error:', e)
            return f"Bot:{self.token}"

    class Meta:
        verbose_name = 'Бот'
        verbose_name_plural = 'Боты'
