from django.urls import path
from django.conf import settings
from . import views
import logging

logger = logging.getLogger(__name__)

webhook_paths = []

manager = settings.TELEGRAM_MANAGER

webhook_base = manager.get('WEBHOOK_URL_PREFIX', '/')
if webhook_base.startswith("/"):
    webhook_base = webhook_base[1:]
if webhook_base == '/':
    webhook_base = ''
elif not webhook_base.endswith("/"):
    webhook_base += "/"

urlpatterns = [
    path('allbots/', views.home, name='django-telegrambot'),
    path('{}<str:bot_token>/'.format(webhook_base), views.webhook, name='webhook'),
    path('{}<str:bot_token>/stop'.format(webhook_base), views.stop, name='webhook-stop-bot'),
    path('{}<str:bot_token>/start'.format(webhook_base), views.start, name='webhook-start-bot')
]