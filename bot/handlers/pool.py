from telegram import Update
from telegram.ext import CallbackContext

from concurrent.futures import ThreadPoolExecutor

from bot.locale.strings import get_all_languages, get_text as _s
from bot.handlers import main
from bot.handlers.paths import buttons_path


pool: ThreadPoolExecutor = ThreadPoolExecutor(max_workers=61)


def fill_handlers_path(paths: dict, dict_to_fill: dict) -> dict:
    return [[dict_to_fill.update({name: handler}) 
             for name in get_button_names(key)] 
             for key, handler in paths.items()]


def get_button_names(key: str):
    return [_s(key, language) for language in get_all_languages()]


def main_thread(update: Update, context: CallbackContext):
    text = update.effective_message.text
    if text in buttons_path.keys():
        print(text, buttons_path[text])
        pool.submit(buttons_path[text], update, context)
    else:
        pool.submit(main.accept_text_messages, update, context)