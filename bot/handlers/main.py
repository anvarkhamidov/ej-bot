from typing import Optional, Union
from telegram import Update, ReplyKeyboardMarkup, KeyboardButton
from telegram.ext import CallbackContext

from bot.locale.strings import (get_all_languages,
                                get_all_texts,
                                get_lang_from_text,
                                get_emoji as _e,
                                get_text as _s)
from bot.utils import core, views
from web.models import User


def cmd_start(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    received_text = update.effective_message.text
    if received_text and not received_text.startswith('/start'):
        emoji, received_text = received_text.split(" ")
    user: User = core.get_user(update, context)
    languages = get_all_languages()

    if received_text in [get_all_texts(lang)[f'language_{lang}'] for lang in languages]:
        user.lang = get_lang_from_text(received_text)
        user.save()

    if not user.lang and not user.is_registered:
        languages_title = [_s("language", lang_id) for lang_id in languages]
        text = f"{_e('language')} <b>{' | '.join(languages_title)}</b>\n\n"
        for lang_id in languages:
            text += f"{_e(f'language_{lang_id}')} {_s('language_choose', lang_id)}\n"
        keyboard = core.build_menu([f"{_e(f'language_{lang_id}')} {_s(f'language_{lang_id}', lang_id)}" 
                                    for lang_id in languages], 1)
    
    elif update.effective_message.contact:
        contact = update.effective_message.contact
        if user.user_id == contact.user_id:
            num = contact.phone_number
            user.is_registered = True
            user.contact = str(num).replace('+', '') if '+' in num else str(num)
            user.save()
            text, keyboard = views.get_main_menu(user)
        else:
            text = _s('contact_not_match', user.lang)
            keyboard = [[KeyboardButton(text=_s('request_contact_btn', user.lang), request_contact=True)]]

    elif user.lang and not user.is_registered:
        text = _s('request_contact', user.lang)
        keyboard = [[KeyboardButton(text=_s('request_contact_btn', user.lang), request_contact=True)]]

    else:
        text, keyboard = views.get_main_menu(user)

    return update.message.reply_html(text=text, reply_markup=ReplyKeyboardMarkup(keyboard, resize_keyboard=True,
                                                                                 one_time_keyboard=True))


def get_command_from_button(update: Update, context: CallbackContext):
    user = core.get_user(update, context)
    return update.message.reply_text(text=update.effective_message.text,
                                     reply_markup=ReplyKeyboardMarkup(views.get_main_menu(user)[-1], 
                                                                      resize_keyboard=True, 
                                                                      one_time_keyboard=True))


def accept_text_messages(update: Update, context: CallbackContext):
    print(update.effective_message)
    pass