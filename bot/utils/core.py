import typing

import telegram

from web.models import BotAdmin, User
from bot.utils.logging import logger

_bot: typing.Optional[telegram.Bot] = None


def set_bot(bot):
    """Remember a reference to the bot instance."""
    global _bot
    _bot = bot


def notify_devs(text: str):
    try:
        admins = BotAdmin.objects.all()
        for admin in admins:
            _bot.send_message(chat_id=admin.user_id, text=text, parse_mode='HTML')
    except Exception as e:
        logger.error(e)


def build_menu(buttons,
               n_cols,
               header_buttons=None,
               footer_buttons=None):
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, [header_buttons])
    if footer_buttons:
        menu.append([footer_buttons])
    return menu


def get_user(update, context) -> User:
    user_data = dict(user_id=update.effective_chat.id,
                     first_name=update.effective_user.first_name,
                     username=update.effective_user.username)
    user, created = User.objects.get_or_create(user_data)
    return user