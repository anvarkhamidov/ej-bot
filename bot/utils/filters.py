from abc import ABC
from typing import Dict, Union, Optional

from telegram import Message
from telegram.ext import MessageFilter
from bot.locale.strings import get_all_languages, get_text as _s


class FilterMixin(MessageFilter):
    message: Message = None

    @classmethod
    def set(cls, value):
        cls.message = value
    
    @classmethod
    def get(cls):
        return cls.message


class RegisterFilter(FilterMixin):

    def language(self):
        message = self.get()
        for language in get_all_languages():
            if _s(f"language_{language}", language) in message.text:
                return True
        return False
    
    def contact(self):
        return True

    def filter(self, message: Message) -> Optional[Union[bool, Dict]]:
        self.set(message)
        if message.text: 
            return self.language()
        elif message.contact:
            return self.contact()
        return False


detect_language = RegisterFilter()
