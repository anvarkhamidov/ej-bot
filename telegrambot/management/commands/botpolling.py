from threading import Thread
from django.core.management.base import BaseCommand
from telegrambot.apps import TelegrambotConfig
from telegrambot.models import Bot
from telegram import Bot as tgBot
from telegram.ext import Updater
import logging
import typing


class Instances(typing.TypedDict):
    bots: typing.List[tgBot]
    updaters: typing.List[Updater]


class Command(BaseCommand):
    help = 'Run telegram bot in polling mode'
    can_import_settings = True

    def get_updater(self, username=None, token=None):
        updater = None
        if username:
            updater = TelegrambotConfig.get_updater(bot_id=username)
            if not updater:
                self.stderr.write(f"Cannot find default bot with username {username}.")
        elif token:
            updater = TelegrambotConfig.get_updater(bot_id=token)
            if not updater:
                self.stderr.write(f"Cannot find bot with token {token}.")
        return updater

    def handle(self, *args, **kwargs):
        from django.conf import settings

        __bots: Instances = {'bots': [], 'updaters': []}

        # Enable Logging
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
        logger = logging.getLogger("telegrambot")
        logger.setLevel(logging.INFO)
        console = logging.StreamHandler()
        console.setLevel(logging.INFO)
        console.setFormatter(logging.Formatter('%(name)s - %(levelname)s - %(message)s'))
        logger.addHandler(console)

        for bot in Bot.objects.all():
            updater = self.get_updater(token=bot.token)
            if not updater:
                self.stderr.write("Bot not found")
                return
            __bots['bots'].append(updater.bot)
            __bots['updaters'].append(updater)

        if not __bots['bots']:
            self.stderr.write("Cannot find bots settings")
            return

        for i, bot, updater in zip(range(len(__bots['bots'])), __bots['bots'], __bots['updaters']):
            if updater:
                updater.start_polling()
                self.stdout.write(f"[{i}] BOT RUNNING. USERNAME: {updater.bot.username}")
                updater.idle()
