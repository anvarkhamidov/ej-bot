from telegram.ext.dispatcher import Dispatcher

import logging

from bot.handlers import register_handlers

logger = logging.getLogger(__name__)


def main(dp: Dispatcher):
    bot = register_handlers(dp)
