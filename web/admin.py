from django.contrib import admin
from django.contrib.auth.models import Group
from django.http.request import HttpRequest
from .models import *

admin.site.unregister(Group)
admin.site.site_header = "botPANEL"
admin.site.site_title = "botPANEL"
admin.site.index_title = "Добро пожаловать"


class UserAdmin(admin.ModelAdmin):
    """Модель пользователей"""

    def has_add_permission(self, request):
        return False

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    list_display = ['first_name', 'username', 'contact', 'lang', 'user_id']
    search_fields = ['username', 'first_name', 'contact', 'lang']
    list_filter = ['lang']
    exclude = ['user_id']

    class Meta:
        model = User


admin.site.register(User, UserAdmin)


class CategoryAdmin(admin.ModelAdmin):
    """Модель категорий"""
    list_display = ['title']

    def has_delete_permission(self, request: HttpRequest, obj=None) -> bool:
        return True

    class Meta:
        model = Category


admin.site.register(Category, CategoryAdmin)


class ProductAdmin(admin.ModelAdmin):
    """Модель продуктов здорового питания"""

    def status_list(self, obj):
        d_status = {0: "❌", 1: "✅"}
        return d_status[obj.status]

    status_list.short_description = 'Статус'

    def description_text_list(self, obj):
        description = """{}""".format(obj.description_ru)
        for rep in ['<b>', '</b>', '<code>', '</code>', '<i>', '</i>']:
            description = description.replace(rep, '')
        return description[:50] + '...'

    description_text_list.short_description = 'Описание'

    list_display = ['title_ru', 'code_catalog', 'note', 'unit_ru', 'description_text_list', 'price', 'category',
                    'status_list']
    search_fields = ['title_ru', 'code_catalog', 'note', 'unit_ru', 'description_ru', 'price', 'category__title_ru']
    list_filter = ['category', 'status']

    def has_delete_permission(self, request: HttpRequest, obj=None) -> bool:
        if obj:
            if obj.code_catalog == '0000':
                return False
        return True

    class Meta:
        model = Product


admin.site.register(Product, ProductAdmin)


class OrderAdmin(admin.ModelAdmin):
    """Модель заказов"""

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def order_text_list(self, obj):
        food_text = """{}""".format(obj.order_text)
        for rep in ['<b>', '</b>', '<code>', '</code>', '<i>', '</i>', '--------------------']:
            food_text = food_text.replace(rep, '')
        return food_text[:50] + '...'

    order_text_list.short_description = 'Заказ'

    def list_count(self, obj):
        return "Заказ - #{}".format(obj.count)

    list_count.short_description = '№ Номер'

    def list_status(self, obj):
        d_status = {0: "🛒", 1: "✅", 2: "⌛", 3: "❌"}
        return d_status[obj.status]

    list_status.short_description = 'Статус заказа'

    list_display = ['list_count', 'first_name', 'contact', 'order_text_list', 'full_price', 'type_payment',
                    'date', 'list_status']
    search_fields = ['order_text', 'full_price', 'first_name', 'contact']
    exclude = ['user_id', 'date', 'message_id_chat', 'type_payment', 'status', 'count']
    list_filter = ['status', 'type_payment']

    class Meta:
        model = Order


admin.site.register(Order, OrderAdmin)


class BotAdminAdmin(admin.ModelAdmin):
    """Администраторы бота"""
    list_display = ['user_id']
    search_fields = ['user_id']

    class Meta:
        model = BotAdmin


admin.site.register(BotAdmin, BotAdminAdmin)


class DeliveryParamsAdmin(admin.ModelAdmin):
    """Параметры доставки"""

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    # list_display = ['title', 'description', 'slug', 'value']
    list_display = ['title', 'description', 'value']
    exclude = ['description', 'slug']

    class Meta:
        model = DeliveryParams


admin.site.register(DeliveryParams, DeliveryParamsAdmin)


class BotSettingsAdmin(admin.ModelAdmin):
    """Параметры доставки"""

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_add_permission(self, request):
        return True

    def has_delete_permission(self, request, obj=None):
        return False

    # list_display = ['title', 'description', 'slug', 'value']
    list_display = ['title', 'description', 'value', 'slug']
    exclude = ['description']

    class Meta:
        model = DeliveryParams


admin.site.register(BotSettings, BotSettingsAdmin)
