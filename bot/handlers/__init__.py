from telegram import Bot
from telegram.ext import Dispatcher, CommandHandler, MessageHandler, Filters

from bot.handlers import main, error
from bot.handlers.pool import fill_handlers_path
from bot.utils import core, filters
from bot.handlers.paths import buttons_path


temp_paths = {
    'overview': main.get_command_from_button,
    'menu_order': main.get_command_from_button,
    'menu_bot_info': main.get_command_from_button,
    'menu_cart': main.get_command_from_button,
}

fill_handlers_path(temp_paths, buttons_path)


def register_handlers(dp: Dispatcher) -> Bot:
    core.set_bot(dp.bot)
    dp.add_handler(CommandHandler("start", main.cmd_start))

    dp.add_handler(MessageHandler(filters.detect_language, main.cmd_start))
    dp.add_handler(MessageHandler(Filters.text, pool.main_thread))

    dp.add_error_handler(error.error_handler)
    return dp.bot
