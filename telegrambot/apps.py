from django.apps import AppConfig, apps
from django.conf import settings
from django.db import OperationalError
from django.utils.module_loading import module_has_submodule
from telegram import Bot
from telegram.utils.request import Request
from telegram.ext import Updater, messagequeue as mq
from telegram.error import InvalidToken, TelegramError
from telegrambot.mqbot import MQBot
import logging
import importlib
import telegram
import os

logger = logging.getLogger(__name__)

TELEGRAM_BOT_MODULE_NAME = 'telegrambot'
WEBHOOK_MODE, POLLING_MODE = range(2)
manager = settings.TELEGRAM_MANAGER


class classproperty(property):
    def __get__(self, obj, objtype=None):
        return super(classproperty, self).__get__(objtype)

    def __set__(self, obj, value):
        super(classproperty, self).__set__(type(obj), value)

    def __delete__(self, obj):
        super(classproperty, self).__delete__(type(obj))


def get_bot_by_token(token) -> telegram.Bot or None:
    for b in TelegrambotConfig.bots:
        if b.token == token:
            return b
    return None


def run_bot_command(method_name, bot_token, execute=True):
    module_name = settings.BOT_RUNNER_MODULE
    try:
        m = importlib.import_module(module_name)
        if execute and hasattr(m, method_name):
            dispatcher = TelegrambotConfig.get_dispatcher(bot_id=bot_token)
            if not dispatcher:
                logger.error(f"Updater for token not found: {bot_token}")
                return False
            if bot_token in TelegrambotConfig.used_tokens:
                getattr(m, method_name)(dispatcher)
                logger.info(f"Run {dispatcher.bot.username}:{method_name}()")
            else:
                logger.error(f"This bot is not running: {dispatcher.bot.username}")
                return False
    except Exception as e:
        logger.error(f"{module_name}: {repr(e)}")
        return False

    return True


def module_imported(module_name, method_name, execute):
    try:
        m = importlib.import_module(module_name)
        if execute and hasattr(m, method_name):
            logger.info('Run {}.{}()'.format(module_name, method_name))
            if len(TelegrambotConfig.used_tokens) > 0:
                for _token in TelegrambotConfig.used_tokens:
                    _dp = TelegrambotConfig.get_dispatcher(bot_id=_token)
                    getattr(m, method_name)(_dp)
        else:
            logger.info('Run {}'.format(module_name))

    except ImportError as er:
        logger.error('{} : {}'.format(module_name, repr(er)))
        return False

    return True


class TelegrambotConfig(AppConfig):
    name = 'telegrambot'
    mode = None
    ready_run = False
    bot_tokens = []
    bot_usernames = []
    bots = []
    updaters = []
    dispatchers = []
    used_tokens = set()

    @classmethod
    def add_token(cls, token):
        # print("Getting value default dispatcher")
        cls.used_tokens.add(token)
        return cls.used_tokens

    @classmethod
    def remove_token(cls, token):
        # print("Getting value default updater")
        cls.used_tokens.remove(token)
        return cls.used_tokens

    @classmethod
    def get_dispatcher(cls, bot_id=None, safe=True):
        if bot_id is None:
            cls.used_tokens.add(cls.bot_tokens[0])
            return cls.dispatchers[0]
        else:
            try:
                index = cls.bot_tokens.index(bot_id)
            except ValueError:
                if not safe:
                    return None
                try:
                    index = cls.bot_usernames.index(bot_id)
                except ValueError:
                    return None
            cls.used_tokens.add(cls.bot_tokens[index])
            return cls.dispatchers[index]

    @classmethod
    def get_bot(cls, bot_id=None, safe=True):
        if bot_id is None:
            if safe:
                return cls.bots[0]
            else:
                return None
        else:
            try:
                index = cls.bot_tokens.index(bot_id)
            except ValueError:
                if not safe:
                    return None
                try:
                    index = cls.bot_usernames.index(bot_id)
                except ValueError:
                    return None
            return cls.bots[index]

    @classmethod
    def get_updater(cls, bot_id=None, safe=True):
        if bot_id is None:
            return cls.updaters[0]
        else:
            try:
                index = cls.bot_tokens.index(bot_id)
            except ValueError:
                if not safe:
                    return None
                try:
                    index = cls.bot_usernames.index(bot_id)
                except ValueError:
                    return None
            return cls.updaters[index]

    @classmethod
    def process_bot(cls, bot):
        token = bot.token
        allowed_updates = bot.allowed_updates
        timeout = bot.timeout
        proxy = bot.proxy
        mode = WEBHOOK_MODE
        if manager.get('MODE', 'WEBHOOK') == 'POLLING':
            mode = POLLING_MODE

        if mode == WEBHOOK_MODE:
            webhook_site = manager.get('WEBHOOK_URL')
            if not webhook_site:
                logger.warning('Required TELEGRAM_WEBHOOK_SITE missing in settings')
                return
            if webhook_site.endswith("/"):
                webhook_site = webhook_site[:-1]

            webhook_base = manager.get('WEBHOOK_URL_PREFIX', '/')
            if webhook_base.startswith("/"):
                webhook_base = webhook_base[1:]
            if webhook_base.endswith("/"):
                webhook_base = webhook_base[:-1]

            try:
                if bot.message_queue_enabled:
                    q = mq.MessageQueue(all_burst_limit=bot.message_all_burst_limit,
                                        all_time_limit_ms=bot.message_all_time_limit_ms)
                    if proxy:
                        request = Request(proxy_url=proxy['proxy_url'],
                                          urllib3_proxy_kwargs=proxy['urllib3_proxy_kwargs'],
                                          con_pool_size=bot.message_request_con_pool_size)
                    else:
                        request = Request(con_pool_size=bot.message_request_con_pool_size or 8)
                    b = MQBot(token, request=request, mqueue=q)
                else:
                    request = Request(con_pool_size=8)
                    if proxy:
                        request = Request(proxy_url=proxy['proxy_url'],
                                          urllib3_proxy_kwargs=proxy['urllib3_proxy_kwargs'])
                    b = Bot(token=token, request=request)

                updater: Updater = Updater(bot=b)

                _bot = updater.bot

                updater.dispatcher.workers = 0
                updater.dispatcher.update_queue = None

                TelegrambotConfig.updaters.append(updater)
                TelegrambotConfig.dispatchers.append(updater.dispatcher)

                max_connections = bot.webhook_max_connections
                hook_url = '{}/{}/{}/'.format(webhook_site, webhook_base, token)
                setted = _bot.set_webhook(url=hook_url, timeout=timeout, max_connections=max_connections,
                                          allowed_updates=allowed_updates)
                webhook_info = _bot.getWebhookInfo()
                real_allowed = webhook_info.allowed_updates if webhook_info.allowed_updates else ["ALL"]

                bot.more_info = webhook_info
                logger.info(f'Telegram Bot <{_bot.username}> setting webhook [ {webhook_info.url} ] '
                            f'max connections:{webhook_info.max_connections} allowed updates:{real_allowed} '
                            f'pending updates:{webhook_info.pending_update_count}: {setted}')

            except InvalidToken:
                logger.error('Invalid Token : {}'.format(token))
                return
            except TelegramError as er:
                logger.error('Error : {}'.format(repr(er)))
                return

        else:
            try:
                updater = Updater(token=token)
                _bot = updater.bot
                _bot.delete_webhook()
                TelegrambotConfig.updaters.append(updater)
                TelegrambotConfig.dispatchers.append(updater.dispatcher)
                TelegrambotConfig.used_tokens.add(token)
            except InvalidToken:
                logger.error(f'Invalid token: {token}')
                return
            except TelegramError as e:
                logger.error(f'Telegram Error: {e}')
                return

        TelegrambotConfig.bots.append(_bot)
        TelegrambotConfig.bot_tokens.append(token)
        TelegrambotConfig.bot_usernames.append(_bot.username)
        TelegrambotConfig.used_tokens.add(token)

    def ready(self):
        try:
            if not os.environ.get('RUN_MAIN') != 'true':
                return

            from .models import Bot

            self.mode = WEBHOOK_MODE
            if manager.get('MODE', 'WEBHOOK') == 'POLLING':
                self.mode = POLLING_MODE

            modes = ['WEBHOOK', 'POLLING']
            logger.error('\n\nTelegram-Manager started on <{} mode>'.format(modes[self.mode].lower()))

            __bots = Bot.objects.all()

            for __bot in __bots:
                TelegrambotConfig.process_bot(__bot)

            # import telegram bot handlers for all INSTALLED_APPS
            for app_config in apps.get_app_configs():
                if module_has_submodule(app_config.module, TELEGRAM_BOT_MODULE_NAME):
                    module_name = '%s.%s' % (app_config.name, TELEGRAM_BOT_MODULE_NAME)
                    if module_imported(module_name, 'main', True):
                        logger.warning(f'Loaded {len(__bots)} bots')

        except OperationalError as e:
            logger.error(f'Error in bots.apps: {e}')
            pass


