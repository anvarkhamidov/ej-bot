import os
from dotenv import load_dotenv

load_dotenv()

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


class Config:
    __config = {
        'secret_key': str(os.getenv('SECRET_KEY')),
        'debug': bool(int(str(os.getenv('DEBUG')))),
        'webhook_site': str(os.getenv('WEBHOOK_SITE')),
        'bot_token': str(os.getenv('BOT_TOKEN')),
        'telegram_mode': str(os.getenv('TELEGRAM_MODE'))
    }
    _available_telegram_mode = ['polling', 'webhook']

    @staticmethod
    def get(key):
        try:
            return Config.__config[key]
        except:
            raise KeyError(f"Key `{key}` does not exist in config")

    @staticmethod
    def set(key, value):
        try:
            Config.__config[key] = value
            return Config.get(key)
        except:
            raise KeyError(f"Key `{key}` does not exist in config")

    @staticmethod
    def validate():
        empty_vars = []

        for key, value in Config.__config.items():
            if not value and key != 'debug':
                empty_vars.append(key)
            elif key == 'telegram_mode' and value.lower() not in Config._available_telegram_mode:
                raise NameError(f"TELEGRAM_MODE can be only WEBHOOK or POLLING.")


        if empty_vars:
            raise NameError(f'ERROR: Variables {", ".join(empty_vars)} are empty.')