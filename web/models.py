from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.text import slugify
from django.conf import settings
from psqlextra.models import PostgresModel
from localized_fields.models import LocalizedModel
from localized_fields.fields import LocalizedField


def photo_validator(value):
    """Валидирование фото для продуктов"""
    limit_mb = 2
    if value.file.size > limit_mb * 1024 * 1024:
        raise ValidationError("Фото не должно быть больше 2мб")

    if str(value.file).split('.')[1] not in ['jpg', 'jpeg', 'png']:
        raise ValidationError('''Фото должно быть в формате jpg, jpeg, png''')


class User(PostgresModel):
    """Модель пользователей"""
    user_id = models.IntegerField(verbose_name='ID пользователя')
    username = models.CharField(max_length=100, verbose_name='User_name', null=True, blank=True)
    first_name = models.CharField(max_length=100, verbose_name='Имя')
    contact = models.CharField(max_length=100, verbose_name='Контакт', null=True, blank=True)
    is_registered = models.BooleanField(verbose_name="Зарегистрирован?", default=False)
    lang = models.CharField(max_length=100, verbose_name='Язык', choices=(('ru', 'Русский'), ('uz', 'Узбекский')),
                            null=True)

    def __str__(self):
        return self.first_name[:50]

    class Meta:
        verbose_name = "клиент"
        verbose_name_plural = "Клиенты"


class Category(LocalizedModel):
    """Модель категорий"""
    title = LocalizedField(max_length=100, verbose_name='Категория')
    slug = models.SlugField(blank=True, null=True)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='children', on_delete=models.CASCADE)

    class Meta:
        unique_together = ('slug', 'parent')
        verbose_name = "категория"
        verbose_name_plural = "Категории"

    def __str__(self):
        full_path = [self.title_ru]
        k = self.parent
        while k is not None:
            full_path.append(k.title_ru)
            k = k.parent
        return ' -> '.join(full_path[::-1])

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title_ru)
        super(Category, self).save(*args, **kwargs)


class Product(PostgresModel):
    """Модель продуктов"""
    category = models.ForeignKey('Category', on_delete=models.SET_NULL, verbose_name='Категория', null=True)

    title_ru = models.CharField(max_length=100, verbose_name='Продукт', help_text='Русский')
    title_uz = models.CharField(max_length=100, verbose_name='Продукт', help_text='Узбекский')

    code_catalog = models.CharField(max_length=100, verbose_name='Код по каталогу')
    note = models.CharField(max_length=100, verbose_name='Примечание')

    unit_ru = models.CharField(max_length=100, verbose_name='Единица измерения', help_text='Русский')
    unit_uz = models.CharField(max_length=100, verbose_name='Единица измерения', help_text='Узбекский')

    description_ru = models.TextField(max_length=1000, verbose_name='Описание', help_text='Русский')
    description_uz = models.TextField(max_length=1000, verbose_name='Описание', help_text='Узбекский')

    price = models.IntegerField(verbose_name='Цена', validators=[MinValueValidator(1000), MaxValueValidator(95000000)])
    status = models.IntegerField(verbose_name='Статус', choices=((0, 'Отключенно'), (1, 'Включенно')), default=1)
    image = models.ImageField(verbose_name="Изображение продукта", default='defaults/product.jpg')

    def __str__(self):
        return self.title_ru

    class Meta:
        verbose_name = "товар"
        verbose_name_plural = "Товары"


class CountOrder(PostgresModel):
    """Номер заказа"""
    count = models.IntegerField(verbose_name='Номер заказа')


class Order(PostgresModel):
    """Заказы"""
    count = models.IntegerField(verbose_name='№ Номер')
    user_id = models.IntegerField(verbose_name='ID пользователя')
    first_name = models.CharField(max_length=100, verbose_name='Имя')
    contact = models.CharField(max_length=100, verbose_name='Контакт')
    order_text = models.TextField(verbose_name='Заказ')
    full_price = models.IntegerField(verbose_name='Цена')
    date = models.CharField(max_length=100, verbose_name='Дата / Время')
    type_payment = models.CharField(max_length=100, verbose_name='Тип оплаты',
                                    choices=(('cash', 'Наличные'), ('online', 'Перечисление'), ('payme', 'PayMe'),
                                             ('click', 'Click')))
    status = models.IntegerField(verbose_name='Статус заказа',
                                 choices=((0, "🛒 Новый"), (1, "✅ Принят"), (2, "⌛ Ожидание"), (3, "❌ Отклонен")))

    def __str__(self):
        return self.order_text[:10]

    class Meta:
        verbose_name = "заказ"
        verbose_name_plural = "Заказы"


class BotAdmin(PostgresModel):
    """Администраторы бота"""
    user_id = models.CharField(verbose_name='user_id - админа', max_length=100)

    def __str__(self):
        return self.user_id

    class Meta:
        verbose_name = "администратор"
        verbose_name_plural = "Администраторы"


class DeliveryParams(PostgresModel):
    """Параметры для доставки"""
    title = models.CharField(max_length=100, verbose_name='Параметр')
    description = models.TextField(max_length=1000, verbose_name='Описание')
    slug = models.CharField(max_length=100, verbose_name='slug')
    value = models.FloatField(verbose_name='Значение')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "параметр"
        verbose_name_plural = "Параметры доставки"


class BotSettings(PostgresModel):
    """Параметры бота"""
    title = models.CharField(max_length=100, verbose_name='Параметр')
    description = models.TextField(max_length=1000, verbose_name='Описание')
    slug = models.CharField(max_length=100, verbose_name='slug')
    value = models.CharField(max_length=255, verbose_name='Значение', blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "параметр"
        verbose_name_plural = "Параметры бота"
